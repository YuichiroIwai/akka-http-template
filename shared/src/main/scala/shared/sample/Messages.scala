package shared.sample

object Messages {
  val server = "Hello Server"
  val react = "Hello React"
  val api = "Hello API"
}
