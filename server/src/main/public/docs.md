class: title

# akka-http + scala-js + AWS

---

## with EC2

--

- Install Java8
```bash
$ sudo yum install java-1.8.0-openjdk
$ sudo alternatives --config java
```

--

- Install sbt  
<sup>http://www.scala-sbt.org/release/docs/ja/Installing-sbt-on-Linux.html</sup>

--

- clone project  
```bash
$ git clone https://bitbucket.org/YuichiroIwai/akka-http-template
```

--

- run  
```bash
$ sbt run
```

---

## with ALB

--

- WebSocketをサポートしている
    - nginxなどのproxyが不要

--

- セキュリティーグループの設定&ルーティング
    - EC2にはALBからのアクセスのみを許可
    - ポートは9000番(設定ファイルで変更可能)

|対象|In|Out|
|---|---|---|
|ALB|Web(80)|All|
|EC2|ALB(9000)|All|

---

## and Route53

--

- ドメインの取得、管理、各種レコードの書き換えなど
    - <sup>他で取得・管理してもいいんですが、AWS内のサービスとの連携も簡単なので、ここで一括管理している</sup>

--

- ALBにドメインを割り当てる
    - <sup>これでレコードが浸透すれば独自ドメインでアクセスできる</sup>

---

class: blank center middle

## EC2の場合はSSHして若干の作業が必要

---

class: blank center middle

## Elastic Beanstalkの利用

---

## Elastic Beanstalkとは？

--

- コードをアップロードするだけで、デプロイ、起動、バランシング、オートスケール等々をやってくれる  
<sup>https://aws.amazon.com/jp/elasticbeanstalk/</sup>

--

- Java, .NET, PHP, Node.js, Python, Ruby, Go, そしてDockerをサポート

--

- JVM系の場合はjarを含むzipファイルとしてアップロードすればOK

---

## 下準備

--

- プロダクトコード + 依存系 + 起動スクリプトをまとめたアーカイブの作成
    - `sbt-native-packager` がサポート
```bash
$ sbt universal:packageBin
```

--

- 出来上がったアーカイブに起動設定のための `Procfile` をアーカイブ内のルート直下設置する
    - これは `server/files/Procfile` に設置済み
    - `build.sbt` の設定でアーカイブ作成時に直下にコピーしている
```scala
  mappings in Universal += file("server/files/Procfile") -> "Procfile",
```

---

## deploy

--

- Elastic Beanstalkで新しいアプリケーションを作成

--

- 環境枠にはウェブサーバを選択

--

- プラットフォームにはJavaを選択
- 最低限の構成なら、単一インスタンスで

--

- アプリケーションコードには、作成したzipをアップロード

--

- 環境情報はそのまま
- その他のリソースは必要ないのでそのまま

--

- インスタンスタイプは必要に応じて変更
- リモートログインするならキーペアを選択

--

- インスタンスが起動され、すぐに動作する環境が手に入る
- ドメインもRoute53から簡単に割当て可能

---
class: blank center middle

# Project Structure

---

## 構成

以下の3つのプロジェクトから構成されている

- server ... サーバ側実装。 `akka-http` によるHTTPプロトコルを提供。
- client ... ブラウザ側実装。 `scala-js` によるJavaScriptを提供。
- shared ... サーバ/ブラウザで共有する。API定義などをここに置く。

```txt
build.sbt                  ... ビルド設定ファイル
client/src/main/scala/     ... JS用コード置き場
server/src/main/scala/     ... サーバ用コード置き場
                twirl/     ... テンプレート置き場
                resources/ ... 設定ファイル等
shared/src/main/scala/     ... 共有コード置き場
```

---

## プラグイン

以下のプラグインを利用している。

<sup>`project/plugins.sbt` 参照</sup>

```scala
sbt-revolver        ... 開発中にソース変更を検知して自動再起動
sbt-assembly        ... プロジェクトを単一jarにまとめる  
sbt-native-packager ... パッケージ作成  
sbt-scalajs         ... ScalaでJavaScriptを提供  
sbt-web-scalajs     ... scala-jsをWebに統合  
sbt-twirl           ... テンプレートエンジン
```

---
class: blank center middle

# Server Implementation

---

## akka-http

--

- AkkaによるHTTPプロトコル実装  
<sup>http://doc.akka.io/docs/akka-http/current/</sup>
    - akka-streamによるストリーム処理

--

- ルーティングDSL
    - パスやメソッドによる振りわけをシンプルに書ける 

```scala
    get {
      complete(sample.html.index.render(Messages.server))
    }
```

--

- WebSocket対応
    - メッセージの受信を入力、メッセージの送信を出力とするFlowとして実装
    - `Flow.fromSinkAndSource` を利用し、入力と出力を別処理にする

---

## autowire

--

- いわゆるRPC(Remote Procedure Call)
    - APIを共有のインターフェース(Scala実装上はtrait)とする
    - Server側はこのインターフェースを実装

--

- シリアライズライブラリを選択可能
    - このプロジェクトでは `boopickle` を利用

```scala
object AutowireServer extends autowire.Server[ByteBuffer, Pickler, Pickler] {
  override def read[R: Pickler](p: ByteBuffer): R = Unpickle[R].fromBytes(p)
  override def write[R: Pickler](r: R): ByteBuffer = Pickle.intoBytes(r)
}
```

--

- APIに対するルーティングも行なってくれる
    - `akka-http` のルーティングDSLに組み合わせて活用

---

## ServerContext

--

- Server側の状態と処理を非同期に管理するためのクラス
    - APIの処理、ユーザの情報、WebSocket経由のコマンド実行
    - AkkaのActorとして実装することで、独立性を担保

--

- APIレスポンスを非同期で返すためにPromiseを利用

```scala
  // ApiImpl側
  override def users(): Future[Seq[String]] = {
    val promise = Promise[Seq[String]]
    context ! Users(promise)
    promise.future
  }
```

```scala
    // ServerContext側
    case Users(promise) =>
      promise.success(nicknames.values.toSeq)
```

---
class: blank center middle

# Client ImplEmentation

---

## scala-js

--

- ScalaコードをJSにコンパイルしてくれるAltJS  
<sup>https://www.scala-js.org/</sup>

--

- チュートリアルとしてはこちらがおすすめ  
<sup>http://www.lihaoyi.com/hands-on-scala-js/</sup>

--

- マルチスレッドなど環境依存の部分を覗き、かなりのコードがそのまま動く
- 既存のJSライブラリに対するファサードも充実している

--

- なにより、Serverサイドとコードを共有できることが素晴らしい

---

## scalajs-react

--

- FacebookのReactJSをscala-jsで使えるようにしたもの
<sup>https://github.com/japgolly/scalajs-react</sup>

--

- Scala的に扱いやすいよう工夫されている

--

```scala
object MessageBoxView {
  case class Props(messageBox: MessageBox, rowCount: Int, dataCount: Int)
  private val component = ScalaComponent.builder[Props]("MessageBoxView")
    .render_P(props =>
      <.textarea(
        ^.rows := props.rowCount,
        ^.className := "form-control",
        ^.value := props.messageBox.lines(props.dataCount)
      )
    )
    .build
  def apply(messageBox: MessageBox, rowCount: Int = 10, dataCount: Int = 10): Scala.Unmounted[Props, _, _] = component(Props(messageBox, rowCount, dataCount))
}
```

---

## autowire

--

- Server側で触れたRPC機構のクライアント側

--

```scala
object AutowireClient extends autowire.Client[ByteBuffer, Pickler, Pickler] {
  override def doCall(req: Request): Future[ByteBuffer] = {
    dom.ext.Ajax.post(
      url = "/api/" + req.path.mkString("/"),
      data = Pickle.intoBytes(req.args),
      responseType = "arraybuffer",
      headers = Map("Content-Type" -> "application/octet-stream")
    ).map(r => TypedArrayBuffer.wrap(r.response.asInstanceOf[ArrayBuffer]))
  }
  override def read[Result: Pickler](p: ByteBuffer): Result = Unpickle[Result].fromBytes(p)
  override def write[Result: Pickler](r: Result): ByteBuffer = Pickle.intoBytes(r)
}
```

--

```scala
      // users APIをコール
      AutowireClient[Api].users().call()
```

---

## WebSocket

--

- テキスト or バイナリのメッセージを送受信できる

--

- このプロジェクトでは通信用の構造体をバイナリとして送受信
    - クライアント -> サーバはCommand
    - サーバ -> クライアントはEvent

```scala
sealed trait Command
case class SendMessage(body: String) extends Command

sealed trait Event
final case class UserEntered(nickname: String) extends Event
final case class UserLeft(nickname: String) extends Event
final case class MessageGotten(nickname: String, message: String) extends Event
```

---

## AppContext

--

- モデルの状態を操作

--

- ネストされたモデルへのアクセスを効率化するためにMonocleというライブラリのLensを利用  
<sup>https://github.com/julien-truffaut/Monocle</sup>

```scala
case class RootModel(wsMessageBox: MessageBox, apiMessageBox: MessageBox, wsState: WebSocketState = NotYet)
object AppContext {
  val wsMessageBox: Lens[RootModel, MessageBox] = GenLens[RootModel](_.wsMessageBox)
  val apiMessageBox: Lens[RootModel, MessageBox] = GenLens[RootModel](_.apiMessageBox)
  val wsState: Lens[RootModel, WebSocketState] = GenLens[RootModel](_.wsState)
  def addWsMessage(model: RootModel, message: String): RootModel = wsMessageBox.modify(_.add(message))(model)
  def addApiMessage(model: RootModel, message: String): RootModel = apiMessageBox.modify(_.add(message))(model)
  def registering(model: RootModel, nickname: String): RootModel = wsState.set(Registering)(model)
  def connecting(model: RootModel, ws: WebSocket): RootModel = wsState.set(Connecting(ws))(model)
  def connected(model: RootModel, ws: WebSocket): RootModel = addWsMessage(wsState.set(Connected(ws))(model), "接続しました")
  def closed(model: RootModel): RootModel = addWsMessage(wsState.set(Closed)(model), "接続が切れました")
}
```

