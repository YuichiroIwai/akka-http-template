# akka-http-template

akka-httpを利用した最低限のサーバ構成のひな形です。

## 概要

- scala-jsが入っています
    - scalajs-reactも入ってます
    - モデルへのアクセスを楽にするためにMonocleのLensを利用しています
- WebSocketも入っています
    - serverと連動した簡易チャットとして動作します
    - autowireを経由したいくつかのAPIコールも含んでいます
- テンプレートエンジン(twirl)が入っています
- sbt-revolverが入っているので、 `sbt ~re-start` で実行すると更新を検知して再ビルド&再起動します

## 構成

```
build.sbt ... ビルド設定ファイル
client/src/main/scala/ ... JS用コード置き場
server/src/main/scala/ ... サーバ用コード置き場
                twirl/ ... テンプレート置き場
                resources/ ... 設定ファイル等
shared/src/main/scala/ ... 共有コード置き場
```

## 起動方法

- `server/src/main/resources/application.conf` を設置して設定をおこないます
    - 設定は `server/src/main/resources/application-default.conf` を参考にしてください
- `sbt run` を実行します

## パッケージング

- sbt-native-packagerプラグインの機能で、 `sbt universal:packageBin` とすることでユニバーサルパッケージを作成できます