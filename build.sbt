import com.amazonaws.regions.{Region, Regions}

val scalaV = "2.11.8"

lazy val server = (project in file("server")).settings(
  maintainer := "Yuichiro Iwai",
  name := "akka-http-template",
  version := "0.1",
  scalaVersion := scalaV,
  scalaJSProjects := Seq(client),
  pipelineStages in Assets := Seq(scalaJSPipeline),
  compile in Compile := ((compile in Compile) dependsOn scalaJSPipeline).value,
  libraryDependencies ++= Seq(
    "com.typesafe.akka" %% "akka-http" % "10.0.3",
    "com.vmunier" %% "scalajs-scripts" % "1.1.0",
    "ch.qos.logback" % "logback-classic" % "1.1.7",
    "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0",
    "org.webjars" % "bootstrap" % "3.3.6" % Provided
  ),
  WebKeys.packagePrefix in Assets := "public/",
  managedClasspath in Runtime += (packageBin in Assets).value,
  mappings in Universal += file("server/files/Procfile") -> "Procfile",
  mappings in Universal += file("server/files/application-eb.conf") -> "application-eb.conf",

  region in ecr := Region.getRegion(Regions.US_WEST_2),
  repositoryName in ecr := (packageName in Docker).value,
  repositoryTags := Seq(version.value),
  localDockerImage in ecr := (packageName in Docker).value + ":" + (version in Docker).value,
  push in ecr := ((push in ecr) dependsOn(publishLocal in Docker, login in ecr)).value
)
  .enablePlugins(SbtWeb, SbtTwirl, JavaAppPackaging, DockerPlugin, EcrPlugin)
  .dependsOn(sharedJvm)

lazy val client = (project in file("client")).settings(
  scalaVersion := scalaV,
  libraryDependencies ++= Seq(
    "com.github.japgolly.scalajs-react" %%% "core" % "1.0.0-RC2",
    "org.scala-js" %%% "scalajs-dom" % "0.9.1",
    "com.github.julien-truffaut" %%% "monocle-core" % "1.4.0",
    "com.github.julien-truffaut" %%% "monocle-macro" % "1.4.0"
  ),
  jsDependencies ++= Seq(
    "org.webjars.bower" % "react" % "15.4.2" / "react-with-addons.js" minified "react-with-addons.min.js" commonJSName "React",
    "org.webjars.bower" % "react" % "15.4.2" / "react-dom.js" minified "react-dom.min.js" dependsOn "react-with-addons.js" commonJSName "ReactDOM",
    "org.webjars" % "jquery" % "1.11.1" / "jquery.js" minified "jquery.min.js",
    "org.webjars" % "bootstrap" % "3.3.6" / "bootstrap.js" minified "bootstrap.min.js" dependsOn "jquery.js"
  )
).enablePlugins(ScalaJSPlugin, ScalaJSWeb)
  .dependsOn(sharedJs)

lazy val shared = (crossProject.crossType(CrossType.Pure) in file("shared")).settings(
  scalaVersion := scalaV
)
  .jsConfigure(_ enablePlugins ScalaJSWeb)
  .settings(
    libraryDependencies ++= Seq(
      "com.lihaoyi" %%% "autowire" % "0.2.5",
      "me.chrons" %%% "boopickle" % "1.2.5"
    )
  )

lazy val sharedJvm = shared.jvm
lazy val sharedJs = shared.js

onLoad in Global := (Command.process("project server", _: State)) compose (onLoad in Global).value

